country_decisions = {

	convince_the_southern_hags = {
		major = yes
		potential = {
            tag = S70
            OR = {
                religion = night_coven_reformed
                religion = night_coven
            }
		}
		allow = {
            hidden_trigger = {
                tag = S70
            }
            OR = {
                religion = night_coven_reformed
                check_variable = {
                    which = ShadowReformVariable
                    value = 1 
                }
            }
            NOT = {
                check_variable = {
                    which = ShadowReformVariable
                    value = 3
                }
            }
		}
		effect = {
			6730 = {
                create_colony = 1000
            }
            6731 = {
                create_colony = 1000
            }
            6732 = {
                create_colony = 1000
            }
            6733 = {
                create_colony = 1000
            }
            6726 = {
                create_colony = 1000
            }
		}
		ai_will_do = {
			factor = 400
		}
	}
}