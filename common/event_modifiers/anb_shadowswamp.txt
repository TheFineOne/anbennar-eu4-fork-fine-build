#troll conquest 

hag_conquest = {
	local_core_creation = -0.5
	local_unrest = -5
}

simegasa_invasion_turmoil = {
	local_unrest = 10
}

night_coven_reform_1_m = {
	global_missionary_strength = 0.01
	missionaries = 1

	manpower_recovery_speed = 0.1
	global_manpower = 5
	land_forcelimit = 10
}
night_coven_reform_2_m = {
	advisor_cost = -0.1
	advisor_pool = 1

	manpower_recovery_speed = 0.1
	global_manpower = 5
	land_forcelimit = 10
}
night_coven_reform_3_m = {
	leader_cost = -0.1
	free_leader_pool = 1

	manpower_recovery_speed = 0.1
	global_manpower = 5
	land_forcelimit = 10
}
night_coven_reform_4_m = {
	technology_cost = -0.1

	manpower_recovery_speed = 0.1
	global_manpower = 5
	land_forcelimit = 10
}
night_coven_reform_5_m = {
	diplomats = 1
	improve_relation_modifier = 0.1

	manpower_recovery_speed = 0.1
	global_manpower = 5
	land_forcelimit = 10
}	

#yezel mora horde, swamp trolls
yezel_mora_horde_starter = {
	land_maintenance_modifier = -0.75
	global_manpower = 10
	manpower_recovery_speed = 0.5
	land_forcelimit = 20
	land_morale = 0.3
	global_supply_limit_modifier = 0.5

	land_attrition = -0.5
}

yezel_mora_festering_horde = {
	prestige_decay = -0.02
	global_unrest = -2
	manpower_recovery_speed = 0.5
}


yezel_mora_festering_horde2 = {
	army_tradition_decay = -0.02
	prestige_decay = -0.02
	global_unrest = -2
	manpower_recovery_speed = 0.5
}


yezel_mora_festering_horde3 = {
	army_tradition_decay = -0.02
	prestige_decay = -0.02
	global_unrest = -2
	manpower_recovery_speed = 0.5
}
