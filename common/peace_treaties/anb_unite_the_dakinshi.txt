po_unite_the_dakinshi = {
	# The following strings need localization: 
	#		"make_dummy_desc" (uses text processing, supporting bracket notation) ("[FROM.GetName] becomes a [THIS.GetName] Dummy")
	#		"CB_ALLOWED_make_dummy" (uses plain localization) ("Enforced Dumbness")
	#		"PEACE_make_dummy" (uses plain localization, but with support for $TAKER$, $TAKERS$, $GIVER$ and $GIVERS$) ("Become $TAKERS$ Dummy")

	# Values default to 0 or "no" unless otherwise stated
	
	category = 6 # Any number between 0 and 6. Represents the tab under which it will be listed. 6 corresponds to "treaties". This value currently defaults to 6.
	power_projection = vassalized_rival # "power_projection" will only be applied if giver is your rival.
	power_cost_base = 0.0 # Base factor for diplomatic power cost when treaty is not allowed by casus belli. Scales with war score cost.
	prestige_base = 1.0 # Base factor for prestige gain and loss. Scales with war score cost.
	ae_base = 0.0 # Base factor for aggressive expansion. Scales with war score cost.
	warscore_cost = {
		# Which of the giver's provinces' war score costs are included and by how much. All entries default to 0. Any combination of entries can be used. Try negative values at your own risk.
		all_provinces = 0.0 # All the giver's provinces
		no_provinces = 1000.0 # This is just a constant. Equivalent to a single province with with war score cost 1
		owner_keeps = 0.0 # The provinces that are not of any of the types below
		conquered = 1.0 # Provinces that are selected by a Demand Province treaty
		returned_core = 0.0 # Provinces that are selected by a Return Core treaty
		released_nation = 0.0 # Provinces that will become a part of a selected released-to-be nation
		cancelled_subject = 0.0 # This one should not be relevant here since Giver can't be a cancelled subject. Including anyway for completeness
		concede_colonial_region = 0.0 # Ditto
	}
	warscore_cap = 100 # Warscore cost will never go above this value. Only enabled if non-negative. This value defaults to -1.
	requires_demand_independence = yes # Whether this can be demanded by the former subject in an independence war without also demanding independence.
	is_make_subject = no # This will make the treaty mutually exclusive with other treaties that makes the giver a subject, or that explicitly frees the giver from their current overlord.
	requires_is_allowed = no # If set to "yes", treaty will only be visible when explicitly allowed by casus belli. If not, it will be available but cost diplomatic power.
	
	is_visible = { # Defaults to equivalent of "always = yes"
		has_reform = dakinshi_warlord_reform
		OR = {
			war_with = S27
			war_with = S28
			war_with = S29
			war_with = S30
			war_with = S31
			war_with = S32			
		}
	}
	is_allowed = { # Defaults to equivalent of "always = yes"
		always = yes
	}
	effect = { # Defaults to empty effect (which makes error log entry but is safe)
		create_union = FROM
		if = {
			limit = {
				FROM = {
					OR = {
						tag = S27
						any_subject_country = {
							tag = S27
						}
					}
				}
			}
			THIS = {
				#FROM = {
				#	every_subject_country = {
				#		THIS = {
				#			create_subject = {
				#			    subject_type = vassal
				#			    subject = PREV
				#			}
				#		}
				#	}
				#}
				create_union = S27
				country_event = { id = dakinshi.4 }				
			}
		}
		if = {
			limit = {
				FROM = {
					OR = {
						tag = S28
						any_subject_country = {
							tag = S28
						}
					}
				}
			}
			THIS = {
				#FROM = {
				#	every_subject_country = {
				#		THIS = {
				#			create_subject = {
				#			    subject_type = vassal
				#			    subject = PREV
				#			}
				#		}
				#	}
				#}
				create_union = S28
				country_event = { id = dakinshi.5 }				
			}
		}
		if = {
			limit = {
				FROM = {
					OR = {
						tag = S29
						any_subject_country = {
							tag = S29
						}
					}
				}
			}
			THIS = {
				#FROM = {
				#	every_subject_country = {
				#		THIS = {
				#			create_subject = {
				#			    subject_type = vassal
				#			    subject = PREV
				#			}
				#		}
				#	}
				#}
				create_union = S29
				country_event = { id = dakinshi.6 }				
			}
		}
		if = {
			limit = {
				FROM = {
					OR = {
						tag = S30
						any_subject_country = {
							tag = S30
						}
					}
				}
			}
			THIS = {
				#FROM = {
				#	every_subject_country = {
				#		THIS = {
				#			create_subject = {
				#			    subject_type = vassal
				#			    subject = PREV
				#			}
				#		}
				#	}
				#}
				create_union = S30
				country_event = { id = dakinshi.7 }				
			}
		}
		if = {
			limit = {
				FROM = {
					OR = {
						tag = S31
						any_subject_country = {
							tag = S31
						}
					}
				}
			}
			THIS = {
				#FROM = {
				#	every_subject_country = {
				#		THIS = {
				#			create_subject = {
				#			    subject_type = vassal
				#			    subject = PREV
				#			}
				#		}
				#	}
				#}
				create_union = S31
				country_event = { id = dakinshi.8 }				
			}
		}
		if = {
			limit = {
				FROM = {
					OR = {
						tag = S32
						any_subject_country = {
							tag = S32
						}
					}
				}
			}
			THIS = {
				#FROM = {
				#	every_subject_country = {
				#		THIS = {
				#			create_subject = {
				#			    subject_type = vassal
				#			    subject = PREV
				#			}
				#		}
				#	}
				#}
				create_union = S32
				country_event = { id = dakinshi.9 }				
			}
		}
	}
	ai_weight = { # Defaults to always evaluate to 0
		export_to_variable = {
			variable_name = ai_value # Mandatory to have entry called "ai_value". Its value at the end of the ai_weight scope is what will be used
			value = trigger_value:always
		}
		set_variable = {
			ai_value = 2000
		}
		limit = { # if limit evaluates false, ai_weight will evaluate to 0 regardless
			always = yes
		}
	}
}