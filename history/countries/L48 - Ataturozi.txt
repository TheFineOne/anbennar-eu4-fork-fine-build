government = monarchy
add_government_reform = autocracy_reform
government_rank = 2
primary_culture = mazava
religion = avo_nakavy
technology_group = tech_vyzemby

capital = 6634

historical_rival = L46 #Amezajik
historical_rival = L50 #Tsanizadai
historical_rival = L51 #Vumarano
historical_rival = L47 #Mazanosy

1000.1.1 = { set_estate_privilege = estate_mages_organization_state }